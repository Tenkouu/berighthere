FROM python:3.7.5-slim
RUN python -m pip install DateTime
COPY ./test/factorial.py /home
CMD ["python", "/home/factorial.py"]